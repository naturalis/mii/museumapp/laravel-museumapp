## Installation 

Install the PHP packages using composer
```
composer install
```

Install NPM dependencies
```
npm install
```

Create your own copy of the env file
```
cp .env.example .env
```

To prevent HTTPS problems, add to the env:
```
APP_PORT=81
``` 

And change the database settings to:
```
DB_CONNECTION=mysql
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=laravel_museumapp
DB_USERNAME=sail
DB_PASSWORD=password
```

Run the environment using sail
```
sail up
``` 

or else 
```
./vendor/bin/sail up
```

Create an encryption key
```
./vendor/bin/sail php artisan key:generate
```

Restore the backup of the database
```
docker exec -it laravel-museumapp_mysql_1 /bin/bash
cd docker-entrypoint-initdb.d/
mysql -u sail -p laravel_museumapp < museumapp.sql
```

Start the frontend engine Vite
```
./vendor/bin/sail npm run dev
```

Marvel at the splendour that is 
```
http://localhost:81/home
```

To call the api routes like 
```
http://localhost:81/api/brahms
```

you will need to copy the _brahms_ table and call it _brahms2_. Or change the table name in BrahmsController from _brahms2_ to _brahms_ for some overwriting action.

## Important files
API and Webroutes can be found in _/routes/api.php_ and _/routes/web.php_.

_/storage/public_ contains the files

_/resources_ the frontend

_/app_ the models, helpers and controllers

## About Laravel
Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel
Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

You may also try the [Laravel Bootcamp](https://bootcamp.laravel.com), where you will be guided through building a modern Laravel application from scratch.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 2000 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).
The pipeline is configured to scan on leaked secrets.

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

 * `pre-commit autoupdate`
 * `pre-commit install`
