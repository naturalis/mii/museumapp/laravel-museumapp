<?php

/**
 * This is the config file for museumapp specific global variables, like file locations.
 */

return [
    'masterlijst' => '/public/uploads/masterlijst.csv',
    'leenobjecten' => '/public/uploads/leenobjecten.csv',

    'crsTableName' => 'crs2',
    'iucnTableName' => 'iucn2',
    'brahmsTableName' => 'brahms2',
    'topstukkenTableName' => 'topstukken2',
    'ttikTableName' => 'ttik2',
    'leenobjectenTableName' => 'leenobjecten2',

    'nbaHost' => 'https://api.biodiversitydata.nl/v2/specimen/query/?_querySpec=',
    'iucnHost' => 'https://apiv3.iucnredlist.org/api/v3/species/%s?token=',
];
