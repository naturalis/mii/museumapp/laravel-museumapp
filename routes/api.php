<?php

use App\Http\Controllers\BrahmsController;
use App\Http\Controllers\CRSController;
use App\Http\Controllers\FavouritesController;
use App\Http\Controllers\IucnController;
use App\Http\Controllers\LeenobjectenController;
use App\Http\Controllers\NatuurwijzerController;
use App\Http\Controllers\RoomController;
use App\Http\Controllers\TentoonstellingController;
use App\Http\Controllers\TopstukkenController;
use App\Http\Controllers\TTIKController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('topstukken', [TopstukkenController::class, 'get']);

Route::get('brahms', [BrahmsController::class, 'get']);

Route::get('favourites', [FavouritesController::class, 'get']);

Route::get('rooms', [RoomController::class, 'get']);

Route::get('crs', [CRSController::class, 'get']);
Route::get('iucn', [IucnController::class, 'get']);
Route::get('natuurwijzer', [NatuurwijzerController::class, 'get']);
Route::get('leenobjecten', [LeenobjectenController::class, 'get']);
Route::get('tentoonstelling', [TentoonstellingController::class, 'get']);
Route::get('ttik', [TTIKController::class, 'get']);
