<?php

namespace App\Console\Commands;

use App\Http\Controllers\IucnController;
use Illuminate\Console\Command;

class harvest_iucn extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:harvest:iucn';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Harvest data from IUCN';

    /**
     * Execute the console command.
     */
    public function handle() {
        $this->info('Iucn harvester invoked');
        $iucn = new IucnController();
        $iucn->harvest();
        $this->info('Iucn harvester done');
    }
}
