<?php

namespace App\Console\Commands;

use App\Http\Controllers\TTIKController;
use Illuminate\Console\Command;

class harvest_ttik extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:harvest:ttik';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Harvest data from the TTIK';

    /**
     * Execute the console command.
     */
    public function handle() {
        $this->info('TTIK harvester invoked');
        $ttik = new TTIKController();
        $ttik->harvest();
        $this->info('TTIK harvester done');
    }
}
