<?php

namespace App\Console\Commands;

use App\Http\Controllers\CRSController;
use Illuminate\Console\Command;

class harvest_crs extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:harvest:crs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Harvest data from the CRS';

    /**
     * Execute the console command.
     */
    public function handle() {
        $this->info('CRS harvester invoked');
        $crs = new CRSController();
        $crs->harvest();
        $this->info('CRS harvester done');
    }
}
