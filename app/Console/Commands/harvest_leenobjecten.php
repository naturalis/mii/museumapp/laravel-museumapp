<?php

namespace App\Console\Commands;

use App\Http\Controllers\LeenobjectenController;
use Illuminate\Console\Command;

class harvest_leenobjecten extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:harvest:leenobjecten';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Harvest data from the leenobjecten csv';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->info('Leenobjecten harvester invoked');
        $leenobjecten = new LeenobjectenController();
        $leenobjecten->harvest();
        $this->info('Leenobjecten harvester done');
        //
    }
}
