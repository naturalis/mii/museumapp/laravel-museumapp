<?php

namespace App\Console\Commands;

use App\Http\Controllers\BrahmsController;
use Illuminate\Console\Command;

class harvest_brahms extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:harvest:brahms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Harvest data from BRAHMS';

    /**
     * Execute the console command.
     */
    public function handle() {
        $this->info('BRAHMS harvester invoked');
        $brahms = new BrahmsController();
        $brahms->harvest();
        $this->info('BRAHMS harvester done');
    }
}
