<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class RoomController extends Controller {
    private $table_name = 'tentoonstelling';

    public function get() {
        $favourites = DB::table($this->table_name)
            ->selectRaw('Zaal, COUNT(*) as count')
            ->groupBy('Zaal')
            ->orderBy('count', 'desc')
            ->get();

        return $favourites;
    }
    //
}
