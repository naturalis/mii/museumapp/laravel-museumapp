<?php

namespace App\Http\Controllers;

use App\Models\CSV;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller {
    public function __construct() {
        $this->masterlistHelper = \MasterListHelper::instance();
    }

    public function index() {
        $tableData = [];

        $tableData['crs'] = $this->getCrsData();
        $tableData['brahms'] = $this->getBrahmsData();
        $tableData['iucn'] = $this->getIucnData();
        $tableData['topstukken'] = $this->getTopstukkenData();
        $tableData['ttik'] = $this->getTtikData();
        $tableData['tentoonstelling'] = $this->getTentoonstellingsData();
        $tableData['leenobjecten'] = $this->getLeenobjectenData();

        return view('home', [
            'tableData' => $tableData,
        ]);
    }

    /**
     * Harvests relevant data from the Brahms table to display on the frontend
     */
    private function getBrahmsData() {
        $tableName = config('museumapp.brahmsTableName');
        $numSpecimens = DB::table($tableName)->distinct('unitid')->count('unitid');
        $numMultimedia = DB::table($tableName)->count();
        $harvestDate = DB::table($tableName)->pluck('inserted')->first();

        return [
            'numRows' => $numMultimedia,
            'numSpecimens' => $numSpecimens,
            'harvestDate' => $harvestDate,
        ];
    }

    /**
     * Harvests relevant data from the CRS table to display on the frontend
     */
    private function getCrsData() {
        $tableName = config('museumapp.crsTableName');
        $numSpecimens = DB::table($tableName)->distinct('REGISTRATIONNUMBER')->count('REGISTRATIONNUMBER');
        $numMultimedia = DB::table($tableName)->count();
        $harvestDate = DB::table($tableName)->pluck('inserted')->first();

        return [
            'numRows' => $numMultimedia,
            'numSpecimens' => $numSpecimens,
            'harvestDate' => $harvestDate,
        ];
    }

    /**
     * Harvests relevant data from the Iucn table to display on the frontend
     */
    private function getIucnData() {
        $tableName = config('museumapp.iucnTableName');
        $numRows = DB::table($tableName)->count();
        $harvestDate = DB::table($tableName)->pluck('inserted')->first();

        return [
            'numRows' => $numRows,
            'harvestDate' => $harvestDate,
        ];
    }

    /**
     * Harvests relevant data from the Topstukken table to display on the frontend
     */
    private function getTopstukkenData() {
        $tableName = config('museumapp.topstukkenTableName');
        $numRows = DB::table($tableName)->count();
        $harvestDate = DB::table($tableName)->pluck('inserted')->first();

        return [
            'numRows' => $numRows,
            'harvestDate' => $harvestDate,
        ];
    }

    /**
     * Harvests relevant data from the TTIK table
     */
    private function getTtikData() {
        $tableName = config('museumapp.ttikTableName');
        $numRows = DB::table($tableName)->count();
        $harvestDate = DB::table($tableName)->pluck('inserted')->first();

        return [
            'numRows' => $numRows,
            'harvestDate' => $harvestDate,
        ];
    }

    /**
     * Harvests relevant data from the Tentoonstellingsobjecten CSV
     */
    private function getTentoonstellingsData() {
        $numRows = $this->masterlistHelper->getTentoonstellingsobjectenNumbers();

        return [
            'numRows' => $numRows,
            'harvestDate' => '???',
        ];
    }
    
    /**
     * Harvests relevant data from the leenobjecten table to display on the frontend
     */
    private function getLeenobjectenData() {
        $tableName = config('museumapp.leenobjectenTableName');
        $numRows = DB::table($tableName)->count();
        $harvestDate = DB::table($tableName)->pluck('inserted')->first();

        return [
            'numRows' => $numRows,
            'harvestDate' => $harvestDate,
        ];
    }
}
