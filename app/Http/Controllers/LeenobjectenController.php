<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class LeenobjectenController extends Controller {
    private $table_name = 'leenobjecten2';

    public function __construct() {
        $this->utility = \UtilityHelper::instance();
        $this->csv_helper = \CSVHelper::instance();
    }

    public function harvest() {
        DB::table($this->table_name)->truncate();
        $csv = $this->csv_helper->read_csv(config('museumapp.leenobjecten'));

        foreach ($csv as $row) {
            DB::table($this->table_name)->insert([
                'registratienummer' => $row['registratienummer'],
                'geleend_van' => $row['geleend_van'],
                'afbeeldingen' => $row['afbeeldingen'],
            ]);
        }
    }
}
