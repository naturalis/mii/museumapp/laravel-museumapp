<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class BrahmsController extends Controller {
    private string $tableName;

    public function __construct() {
        $this->tableName = config('museumapp.brahmsTableName');
        $this->nba = \NBAHelper::instance();
        $this->masterlistHelper = \MasterListHelper::instance();
    }

    /**
     * Reads the masterlijst CSV file and retrieves BRAHMS data given the registration numbers.
     * First, for every entry in the csv file a query condition is made.
     * Then, the query conditions are combined in batches and send to the NBA. The
     * results are then entered into the database.
     *
     * @author Luuk
     */
    public function harvest(): void {
        // Truncate the table first
        DB::table($this->tableName)->truncate();
        // Retrieve the masterlist items from the csv file
        $registrationNumbers = $this->masterlistHelper->getRegistrationNumbers();
        // Create query conditions given the CSV
        $querySpecConditions = $this->createQuerySpecConditions($registrationNumbers);
        // Run the query conditions on the NBA and store the data in the database
        $results = $this->runQuerySpecConditionsOnNba($querySpecConditions);
        // Store the results in the database
        foreach ($results as $result) {
            if (! empty($result) && ! empty($result['multimediaList'])) {
                foreach ($result['multimediaList'] as $multimedia) {
                    DB::table($this->tableName)->insert([
                        'unitid' => $result['registrationNumber'],
                        'URL' => $multimedia,
                    ]);
                }
            }
        }
    }

    /**
     * Creates query spec conditions for each specimen in the given array.
     *
     * @param $registrationNumbers (array)
     *
     * @author Luuk
     */
    private function createQuerySpecConditions($registrationNumbers): array {
        $querySpecConditionsList = [];
        // Loop through each row in the CSV and find the BRAHMS data. This is done in bursts
        foreach ($registrationNumbers as $registrationNumber) {
            // Create the query spec condition for this specimen
            $registrationNumber = $this->nba->createCondition('sourceSystemId', 'EQUALS_IC', $registrationNumber);
            $source = $this->nba->createCondition('sourceSystem.code', 'EQUALS', 'BRAHMS');
            $registrationNumber['and'] = [$source];
            array_push($querySpecConditionsList, $registrationNumber);
        }

        return $querySpecConditionsList;
    }

    /**
     * Fires the given querySpecConditions in batches to the NBA. Saves the results to the database.
     *
     * @param querySpecConditions (array)
     * @return processedSpecimens (array) with (registration number + multimedia array with urls)
     *
     * @author Luuk
     */
    private function runQuerySpecConditionsOnNba(array $querySpecConditions): array {
        $batches = $this->nba->createNbaBatches($querySpecConditions);
        $results = [];
        foreach ($batches as $batch) {
            $querySpec = $this->nba->querySpec;
            foreach ($batch as $querySpecCondition) {
                array_push($querySpec['conditions'], $querySpecCondition);
            }
            $result = $this->nba->post($querySpec);
            array_push($results, $result['resultSet']);
            // Extract the data from the retrieved NBA object
            foreach ($result['resultSet'] as $specimen) {
                $processedSpecimen = $this->extractDataFromNbaResult($specimen);
                $results[] = $processedSpecimen;
            }
        }

        return $results;
    }

    /**
     * Extracts registration number and multimedia items from the NBA result.
     *
     * @param array (array)
     *
     * @author Luuk
     */
    private function extractDataFromNbaResult(array $result): array {
        $entry = [
            'registrationNumber' => '',
            'multimediaList' => [],
        ];
        if (! empty($result)) {
            $result = $result['item'];
            if (array_key_exists('sourceSystemId', $result)) {
                $entry['registrationNumber'] = $result['sourceSystemId'];
            }
            if (array_key_exists('associatedMultiMediaUris', $result)) {
                $multimedia_array = $result['associatedMultiMediaUris'];
                foreach ($multimedia_array as $multimedia) {
                    array_push($entry['multimediaList'], $multimedia['accessUri']);
                }
            }
        }

        return $entry;
    }
}
