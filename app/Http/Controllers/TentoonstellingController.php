<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class TentoonstellingController extends Controller {
    private $table_name = 'tentoonstelling2';

    public function __construct() {
        $this->utility = \UtilityHelper::instance();
        $this->csv_helper = \CSVHelper::instance();
    }

    //
    public function get() {
        DB::table($this->table_name)->truncate();
        $csv = $this->csv_helper->read_csv('/public/masterlijst.csv');

        foreach ($csv as $row) {
            if (array_key_exists('Registratienummer', $row) && $row['Registratienummer'] != '') {
                DB::table($this->table_name)->insert([
                    'Registratienummer' => $row['Registratienummer'],
                    'Zaal' => $row['Zaal'],
                    'Zaaldeel' => $row['Zaaldeel'],
                    'SCname' => $row['SCname'],
                    'SCname controle' => $row['SCname controle'],
                ]);
            }
        }

        return 200;
    }
}
