<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use voku\helper\HtmlDomParser;

class TopstukkenController extends Controller {
    private $table_name = 'topstukken2';

    private $url = 'https://topstukken.naturalis.nl/';

    private $objects = [];

    public function __construct() {
        $this->utility = \UtilityHelper::instance();
        $this->reaper = \ReaperHelper::instance();
    }

    public function get() {
        DB::table($this->table_name)->truncate();
        $results = $this->import();
        $this->store($results);

        return 200;
    }

    public function import() {
        $topstukken_objects = [];
        $results = [];
        // First, get a list of all Topstukken items
        $index = $this->extractJson($this->url);
        if (! empty($index->grid->items)) {
            foreach ($index->grid->items as $item) {
                $topstukken_objects[$item->id] = $item->slug;
            }
        }
        //$this->utility->print_array($topstukken_objects);
        //$this->utility->print_array($index);
        // Retrieve the content of each Topstuk item
        foreach ($topstukken_objects as $id => $slug) {
            $objectUrl = $this->reaper->setPath($this->url).'object/'.$slug;
            //echo $objectUrl;
            $result = $this->extractJson($objectUrl);
            $result->objectUrl = $objectUrl;
            array_push($results, $result);
            //$this->utility->print_array($result);
            //return $results;
        }

        return $results;
        //$this->utility->print_array($this->objects);
    }

    private function store(array $results) {
        foreach ($results as $object) {
            //$this->utility->print_array($object);
            //return 200;
            $data = (array) $object->specimen->info;
            $data['title'] = $object->specimen->title;

            $description = [];
            if (isset($object->specimen->blocks)) {
                foreach ($object->specimen->blocks as $block) {
                    $description[] = [$block->title ?? '', $block->body ?? ''];
                }
            }
            $data['description'] = json_encode($description);
            //$data['url'] = $object->objectUrl;
            $data['image'] = rtrim($this->url, '/').$object->specimen->image->srcSet->{'1920'};

            DB::table($this->table_name)->insert([
                'description' => $data['description'],
                'title' => $data['title'],
                'registrationNumber' => $data['registrationNumber'],
                'collection' => $data['collection'],
                'country' => $data['country'],
                'scientificName' => $data['scientificName'],
                'year' => $data['year'],
                'expedition' => $data['expedition'],
                'collector' => $data['collector'],
                'url' => $object->objectUrl,
                'image' => $data['image'],
                //'' => $data[''],
            ]);
        }
    }

    /**
     * This assumes that a script with the variable INITIAL_DATA is present containing the
     * complete dataset.
     */
    private function extractJson($url) {
        $html = HtmlDomParser::file_get_html($url);
        foreach ($html->find('script') as $script) {
            if (strpos($script->nodeValue, 'INITIAL_DATA')) {
                $lines = explode("\n", trim($script->nodeValue));
                foreach ($lines as $line) {
                    if (strpos($line, 'INITIAL_DATA')) {
                        $var = trim($this->reaper->fixSimpleHtmlDomReplacements($line));
                    }
                }
            }
        }
        if (empty($var)) {
            //$this->logger->log('Cannot parse data from ' . $this->objectUrl, 1);
            return false;
        }
        $first = strpos($var, '{');
        $json = substr($var, $first, strrpos($var, '}') - $first + 1);

        return json_decode($json);
    }
}
