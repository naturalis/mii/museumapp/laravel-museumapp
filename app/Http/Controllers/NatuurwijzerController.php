<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class NatuurwijzerController extends Controller {
    private $table_name = 'natuurwijzer2';

    public function __construct() {
        $this->utility = \UtilityHelper::instance();
        $this->csv_helper = \CSVHelper::instance();
    }

    //
    public function get() {
        DB::table($this->table_name)->truncate();
        $csv = $this->csv_helper->read_csv('/public/leenobjecten.csv');

        foreach ($csv as $row) {
            DB::table($this->table_name)->insert([
                'registratienummer' => $row['registratienummer'],
                'geleend_van' => $row['geleend_van'],
                'afbeeldingen' => $row['afbeeldingen'],
            ]);
        }

        return 200;
    }
}
