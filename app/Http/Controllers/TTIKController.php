<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class TTIKController extends Controller {
    private $table_name = 'ttik2';

    private $table_name_translations = 'ttik_translations2';

    private $rows = 50;

    private $offset = 0;

    private $pid = 1;

    private $from = '19000101';

    private $total = 0;

    private $names = [];

    private $taxa = [];

    private $totalTaxa = 0;

    private $currentTaxonId;

    private $url = 'https://ttik.linnaeus.naturalis.nl/linnaeus_ng/app/views/webservices/';

    private $url_names = 'https://ttik.linnaeus.naturalis.nl/linnaeus_ng/app/views/webservices/'.'names.php';

    public function __construct() {
        $this->utility = \UtilityHelper::instance();
        $this->csv_helper = \CSVHelper::instance();
        $this->reaper = \ReaperHelper::instance();
    }

    //
    public function harvest() {
        DB::table($this->table_name)->truncate();
        DB::table($this->table_name_translations)->truncate();
        $this->setTotal();

        for ($this->offset = 0; $this->offset < $this->total; $this->offset += $this->rows) {
            $this->setNames();
            $this->setTaxa();
            //$this->utility->print_array($this->taxa);
            $this->insertData();
        }

        //$response = Http::get($this->url);
    }

    /* Keep the taxa array as lean as possible by removing taxa that have been inserted. However!
     The data for a taxon may be incomplete, as we're looping over names, not taxa. Thejkrefore,
     always keep the last taxon in the array. Only when the parameter all has been set,
     all taxa should be inserted.
     */
    private function insertData($all = false) {
        foreach ($this->taxa as $key => $taxon) {
            $scientificName = trim($taxon['uninomial'].' '.$taxon['specific_epithet'].' '.
                $taxon['infra_specific_epithet']);
            end($this->taxa);
            if ($key === key($this->taxa) && ! $all) {
                break;
            }

            $these_descriptions = $taxon['description'];
            unset($taxon['description']);

            //$this->utility->print_array($taxon);
            DB::table($this->table_name)->insert($taxon);

            //if ($this->pdo->insertRow(self::TABLE, $taxon)) {
            $this->reaper->imported++;

            foreach ($these_descriptions as $lang => $this_description) {
                //print_r($this_description);
                if (empty($this_description)) {
                    continue;
                }
                DB::table($this->table_name_translations)->insert([
                    'taxon_id' => $taxon['taxon_id'],
                    'language_code' => $lang,
                    'description' => json_encode($this_description),
                ]);
            }
            unset($this->taxa[$key]);
            //}
        }
    }

    private function setNames() {
        $response = Http::get($this->url_names, [
            'pid' => $this->pid,
            'from' => $this->from,
            'rows' => $this->rows,
            'offset' => $this->offset,
            'all' => 1,
        ]);
        $result = $response->json();
        $this->names = $result['names'];
    }

    private function setTaxa() {
        foreach ($this->names as $name) {
            $id = $name['taxon_id'];
            $common = [];
            $synonyms = [];
            // Add just once for the first iteration of a taxon
            if ($id != $this->currentTaxonId) {
                $this->taxa[$id]['description'] = $this->getTaxonDescription($id);
                $this->taxa[$id]['classification'] = $this->getTaxonClassification($id);
                $this->taxa[$id]['synonyms'] = null;
            }
            // Valid name; add main data
            if ($name['language'] == 'Scientific' && $name['nametype'] == 'isValidNameOf') {
                $this->taxa[$id] = array_merge($this->taxa[$id], $this->stripNameData((array) $name));
                $this->totalTaxa++;
                // Synonyms
            } elseif ($name['language'] == 'Scientific') {
                $synonyms[] = [
                    'uninomial' => $name['uninomial'],
                    'specific_epithet' => $name['specific_epithet'],
                    'infra_specific_epithet' => $name['infra_specific_epithet'],
                    'authorship' => $name['authorship'],
                    'nametype' => $name['nametype'],
                ];
                if (! empty($this->taxa[$id]['synonyms'])) {
                    $synonyms[] = array_merge(json_decode($this->taxa[$id]['synonyms']), $synonyms);
                }
                $this->taxa[$id]['synonyms'] = json_encode($synonyms);
                // Common names in Dutch or English
            } elseif (in_array($name['language'], ['English', 'Dutch'])) {
                $common[] = ['name' => $name['name'], 'nametype' => $name['nametype'], 'remark' => $name['remark']];
                if (! empty($this->taxa[$id][strtolower($name['language'])])) {
                    $common = array_merge(json_decode($this->taxa[$id][strtolower($name['language'])]), $common);
                }
                $this->taxa[$id][strtolower($name['language'])] = json_encode($common);
            }
            $this->currentTaxonId = $id;
        }
    }

    private function stripNameData($name) {
        $fields = [
            'name_id',
            'name',
            'language',
            'language_iso3',
            'last_change',
            'nametype',
            'taxon_valid_name_id',
            'url',
        ];
        foreach ($fields as $field) {
            if (isset($name[$field])) {
                unset($name[$field]);
            }
        }

        return $name;
    }

    private function setTotal() {
        if ($this->total == 0) {
            $response = http::get($this->url_names, [
                'pid' => $this->pid,
                'from' => $this->from,
                'count' => 1,
                'all' => 1,
            ]);
            //print_r($response->json());
            $this->total = $response->json()['count'];
        }

        return $this->total;
    }

    private function getTaxonDescription($taxonId) {
        $description = [];

        foreach (['nl', 'en'] as $lang) {
            $description[$lang] = [];
            foreach ([1, 4, 5] as $cat) {
                $url = $this->url.'taxon_page.php';
                $response = Http::get($url, [
                    'pid' => $this->pid,
                    'taxon' => $taxonId,
                    'cat' => $cat,
                    'lang' => ($lang == 'en' ? '26' : '24'),
                ]);
                //$this->curl->get($url, [ ]);
                $data = $response->json(); //json_decode($this->curl->response);
                if (! empty($data->page->body)) {
                    $description[$lang][] = [
                        'title' => $data->page->title,
                        'body' => $data->page->body,
                        'verified' => empty($data->page->publish) ? '0' : $data->page->publish,
                    ];
                }
            }
        }

        return $description;
    }

    private function getTaxonClassification($taxonId) {
        $url = $this->url.'taxonomy.php';
        $response = Http::get($this->url, [
            'pid' => $this->pid,
            'taxon' => $taxonId,
        ]);
        //$this->curl->get($url, [ ]);
        $data = $response->json(); //json_decode($this->curl->response);
        //$this->curl->get($url, [
        //'pid' => $this->pid,
        //'taxon' => $taxonId,
        //]);
        //$data = json_decode($this->curl->response);
        if (! empty($data->classification) && count($data->classification) > 0) {
            return json_encode($data->classification);
        }

        return null;
    }
}
