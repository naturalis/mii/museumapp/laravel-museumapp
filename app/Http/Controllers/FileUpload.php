<?php

namespace App\Http\Controllers;

use App\Models\File;
use Illuminate\Http\Request;

/**
 * Allows for the uploading of files.
 */
class FileUpload extends Controller {
    private $storage_disk = 'public';

    private $storage_folder = 'uploads';

    // Supported files can be comma separated file extensions. e.g. 'csv,xlsx'
    private $supported_files = 'csv';

    /**
     * Shows the blade view for the file upload
     *
     * @author Luuk
     */
    public function createForm() {
        return view('file-upload');
    }

    /**
     * Uploads the requested file to the storage folder. Validates the given file for its extension.
     *
     * @param req containing the upload request of the file
     *
     * @author Luuk
     */
    public function fileUpload(Request $req) {
        $req->validate([
            'file' => sprintf('required|mimes:%s|max:2048', $this->supported_files),
        ]);
        $fileModel = new File;
        if ($req->file()) {
            $fileName = $req->file->getClientOriginalName();
            $filePath = $req->file('file')->storeAs($this->storage_folder, $fileName, $this->storage_disk);
            $fileModel->file_path = $filePath;
            $fileModel->save();

            $return_string = sprintf('File "%s" has been uploaded', $fileName);

            return back()
                ->with('success', $return_string)
                ->with('file', $fileName);
        }
    }
}
