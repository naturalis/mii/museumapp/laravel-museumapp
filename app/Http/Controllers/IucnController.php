<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class IucnController extends Controller {
    private string $url;

    private string $token;

    private string $tableName;

    public function __construct() {
        $this->nba = \NBAHelper::instance();
        $this->utility = \UtilityHelper::instance();
        $this->csv_helper = \CSVHelper::instance();
        $this->reaper = \ReaperHelper::instance();

        $this->tableName = config('museumapp.iucnTableName');
        $this->url = config('museumapp.iucnHost');
        $this->token = $_ENV['IUCN_TOKEN'];
    }

    public function harvest(): void {
        DB::table($this->tableName)->truncate();

        $species = $this->reaper->getTaxonNames2();
        foreach ($species as $currentSpecimen) {
            // Format the URL for the current specimen
            $url = "{$this->url}{$this->token}";
            $url = sprintf($url, rawurlencode($currentSpecimen));
            // Retrieve data from URL
            $response = Http::get($url);
            $result = $response->json();
            // Store results in a separate array
            if (! empty($result) && ! empty($result['result'])) {
                // From the reponse, the data is stored in Array([result] => [0] => Array())
                $result = $result['result'][0];
                $this->insertData($result);
            }
        }
    }

    /**
     * Writes the given iucn array to the database
     *
     * @param $array with the iucn data from a certain specimen
     *
     * @author Luuk
     */
    private function insertData($array): void {
        echo "Inserting data for {$array['scientific_name']}\n";
        DB::table($this->tableName)->insert([
            'taxonid' => $array['taxonid'],
            'scientific_name' => $array['scientific_name'],
            'assessment_date' => $array['assessment_date'],
            'category' => $array['category'],
            'criteria' => $array['criteria'],
            'population_trend' => $array['population_trend'],
        ]);
    }
}
