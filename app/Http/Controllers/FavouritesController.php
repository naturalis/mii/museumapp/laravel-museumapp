<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class FavouritesController extends Controller {
    private $table_name = 'favourites';

    public function get() {
        $favourites = DB::table($this->table_name)
            ->select('taxon', 'rank')
            ->get();

        return $favourites;
    }
    //
}
