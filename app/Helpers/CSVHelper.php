<?php

namespace App\Helpers;

class CSVHelper {
    /**
     * Reads the masterlijst csv with all tentoonstellingsobjecten to an array.
     *
     * @return tentoonstellingsobjecten (array)
     *
     * @author Luuk
     */
    public function read_csv(string $file) {
        $filePath = storage_path($file);
        $file = fopen($filePath, 'r');
        $header = fgetcsv($file);

        $csv_results = [];
        while ($row = fgetcsv($file)) {
            $csv_results[] = array_combine($header, $row);
        }
        fclose($file);

        return $csv_results;
    }

    public function print_csv(array $csv) {
        echo '<pre>';
        print_r($csv);
    }

    public static function instance() {
        return new CSVHelper;
    }
}
