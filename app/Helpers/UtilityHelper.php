<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;

class UtilityHelper {
    public static function instance() {
        return new UtilityHelper;
    }

    public function is_empty_array(array $array) {
        return empty($array);
    }

    public function print_array($array) {
        echo '<pre>';
        print_r($array);
    }

    /**
     * @param  int  $p
     * @return multitype:multitype:
     *
     * @link http://www.php.net/manual/en/function.array-chunk.php#75022
     */
    public function partition(array $list, $p) {
        $listlen = count($list);
        $partlen = floor($listlen / $p);
        $partrem = $listlen % $p;
        $partition = [];
        $mark = 0;
        for ($px = 0; $px < $p; $px++) {
            $incr = ($px < $partrem) ? $partlen + 1 : $partlen;
            $partition[$px] = array_slice($list, $mark, $incr);
            $mark += $incr;
        }

        return $partition;
    }
}
