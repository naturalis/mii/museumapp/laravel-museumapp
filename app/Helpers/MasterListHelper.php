<?php

namespace App\Helpers;

class MasterListHelper {
    public function __construct() {
        $this->csv_helper = \CSVHelper::instance();
    }

    /**
     * @param csv (csv)
     * @return masterlist_items (array)
     */
    public function getRegistrationNumbers(): array {
        $masterlist_items = [];
        $csv = $this->csv_helper->read_csv(config('museumapp.masterlijst'));
        foreach ($csv as $row) {
            if (array_key_exists('Registratienummer', $row) && $row['Registratienummer'] != '') {
                array_push($masterlist_items, $row['Registratienummer']);
            }
        }

        return $masterlist_items;
    }

    /**
     * Returns the number of tentoonstellingsobjecten currently in the mastercsv file
     *
     * @return count (int) of tentoonstellingsobjecten
     *
     * @author Luuk
     */
    public function getTentoonstellingsobjectenNumbers() {
        $csv = $this->csv_helper->read_csv(config('museumapp.masterlijst'));
        $count = 0;
        foreach ($csv as $row) {
            if ($row['Registratienummer'] != '') {
                $count++;
            }
        }

        return $count;
    }

    public static function instance() {
        return new MasterListHelper;
    }
}
