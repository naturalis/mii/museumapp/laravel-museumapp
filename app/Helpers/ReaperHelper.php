<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class ReaperHelper {
    public $imported = 0;

    public static function instance() {
        return new ReaperHelper;
    }

    public function getSpeciesNames() {
        foreach ($this->getTaxonNames(false) as $taxon) {
            $tmp = array_filter(explode(' ', $taxon));
            if (count($tmp) >= 2) {
                $species[] = ucfirst($tmp[0]).' '.$tmp[1];
            }
        }
        natcasesort($species);

        return array_filter(array_unique($species));
    }

    public function getTaxonNames($bySource = true) {
        $data = $this->getTaxonNames2();
        if (isset($data) && ! $bySource) {
            $flat = [];
            foreach (array_keys($data) as $source) {
                $flat = $flat + $data[$source];
            }
            sort($flat);
        }

        return isset($flat) ? $flat : (isset($data) ? $data : []);
    }

    public function getTaxonNames2($bySource = true) {
        $taxon_names = [];
        $topstukken_results = DB::table('topstukken')
            ->where('scientificName', '!=', '')
            ->pluck('scientificName')
            ->toArray();
        $tentoonstelling_results = DB::table('tentoonstelling')
            ->where('SCName', '!=', '')
            ->pluck('SCName')
            ->toArray();
        $ttik_results = DB::table('ttik')
            ->select('uninomial', 'specific_epithet', 'infra_specific_epithet')
            ->where('specific_epithet', '!=', '')
            ->get()
            ->toArray();

        // Combine the taxon names of the three queries
        foreach ($ttik_results as $taxon) {
            $name = '%s %s %s';
            $taxon_name = trim(sprintf($name, $taxon->uninomial, $taxon->specific_epithet, $taxon->infra_specific_epithet));
            array_push($taxon_names, $taxon_name);
        }

        array_merge($taxon_names, $topstukken_results, $tentoonstelling_results);
        array_unique($taxon_names);

        //echo "<pre>";
        //print_r($taxon_names);
        //foreach($topstukken_results as $taxon) {
        //echo rawurlencode($taxon);
        //}

        //echo "<pre>";
        //print_r($ttik);
        return $taxon_names;
        //$query = "
        //select scientificName as scientific_name, 'topstukken' as source
        //from topstukken
        //where scientificName != ''
        //union select SCName as scientific_name, 'tentoonstelling' as source
        //from tentoonstelling
        //where SCName != ''
        //union select trim(concat(uninomial, ' ', specific_epithet, ' ', infra_specific_epithet))
        //as scientific_name, 'ttik' as source
        //from ttik
        //where specific_epithet != ''
        //order by scientific_name";
        //$stmt = $this->pdo->query($query);
        //while ($row = $stmt->fetch()) {
        //$data[$row['source']][] = $row['scientific_name'];
        //}

        //$stmt = $this->pdo->query("select taxon from natuurwijzer");
        //$natuurwijzer = [];
        //while ($row = $stmt->fetch()) {
        //if (!empty($row['taxon'])) {
        //$natuurwijzer = array_merge($natuurwijzer, (array)json_decode($row['taxon'], true));
        //}
        //}
        //natcasesort($natuurwijzer);
        //$data['natuurwijzer'] = array_unique($natuurwijzer);
        //return isset($data) ? $data : [];
    }

    // Workaround for bug in SimpleHtmlDom
    public function fixSimpleHtmlDomReplacements($str) {
        $domReplaceHelper = [
            'orig' => ['&', '|', '+', '%', '@'],
            'tmp' => [
                '____SIMPLE_HTML_DOM__VOKU__AMP____',
                '____SIMPLE_HTML_DOM__VOKU__PIPE____',
                '____SIMPLE_HTML_DOM__VOKU__PLUS____',
                '____SIMPLE_HTML_DOM__VOKU__PERCENT____',
                '____SIMPLE_HTML_DOM__VOKU__AT____',
            ],
        ];

        return str_replace($domReplaceHelper['tmp'], $domReplaceHelper['orig'], $str);
    }

    public function setPath($path) {
        return rtrim($path, '/').'/';
    }
}
