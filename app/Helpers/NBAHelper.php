<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;

class NBAHelper {
    private string $nba_host;

    // Number of query conditions that are given to the NBA at a time. e.g. n specimens in 1 query.
    private $nba_burst_size = 500;

    // Query spec template
    public $querySpec = [
        'logicalOperator' => 'OR',
        'size' => '50000',
        'conditions' => [],
    ];

    public function __construct() {
        $this->nba_host = config('museumapp.nbaHost');
        $this->utility = \UtilityHelper::instance();
    }

    public static function instance() {
        return new NBAHelper;
    }

    /**
     * Creates an nba query condition given the field, operator and value.
     *
     * @param field (string) to search through. Like a key.
     * @param operator (string)
     * @param value (string) that the given field needs to have
     *
     * @author Luuk
     */
    public function createCondition(string $field, string $operator, string $value): array {
        return [
            'field' => $field,
            'operator' => $operator,
            'value' => $value,
        ];
    }

    /**
     * Performs HTTP post with given object
     *
     * @param querySpec (array) that is to be given to the NBA
     *
     * @author Luuk
     */
    public function post(array $querySpec): array {
        $response = Http::post($this->nba_host, $querySpec);

        return $response->json();
    }

    public function createNbaBatches($querySpecConditions): array {
        $num_batches = $this->calculate_batch_size(count($querySpecConditions));

        return $this->utility->partition($querySpecConditions, $num_batches);
    }

    public function calculate_batch_size($numQueryConditions): int {
        return round($numQueryConditions / $this->nba_burst_size, 0, PHP_ROUND_HALF_UP);
    }
}
