<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CSV extends Model {
    use HasFactory;

    public function lowest(): Attribute {
        return new Attribute(
            get: function () {
                //do whatever you want to do
                return 1;
                //return $modifiedValue;
            });
    }
}
