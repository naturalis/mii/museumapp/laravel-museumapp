<x-app-layout>
    <div class="container mt-5">
        <form action="{{route('fileUpload')}}" method="post" enctype="multipart/form-data">
          <h3 style='padding-bottom:1em' ><b>Upload your CSV files here</b></h3>
            @csrf
            @if ($message = Session::get('success'))
            <div class="alert alert-success" style="max-width:50vw">
                <strong>{{ $message }}</strong>
            </div>
          @endif
          @if (count($errors) > 0)
            <div class="alert alert-danger" style="max-width:50vw">
                <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif
            <div class="custom-file" style="max-width:50vw">
                <input type="file" name="file" class="custom-file-input" id="chooseFile">
                <label class="custom-file-label" for="chooseFile">Select file</label>
            </div>
            <button type="submit" name="submit" class="btn btn-primary btn-block mt-4" style="max-width:50vw">
                Upload Files
            </button>
        </form>
    </div>
</x-app-layout>
