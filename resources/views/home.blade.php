<x-app-layout>
    <div style='padding-left:1em; padding-top: 1em'>       
        <p class='padded-paragraph'>Dit is de museumapp pipeline! Hoera!</p>
  </div>
  <div style='padding:1em'>

    <table>
      <tr>
        <th>Bron</th>
        <th>Aantal</th>
        <th>Harvest date</th>
        <th>Info</th>
      </tr>
      <tr>
        <td>Tentoonstellingsobjecten</td>
        <td>{{ $tableData['tentoonstelling']['numRows'] }}</td>
        <td>{{ $tableData['tentoonstelling']['harvestDate'] }}</td>
        <td>unieke objecten in de masterlist; download csv (vereist google-login & rechten)</td>
      </tr>
      <tr>
        <td>Taxa</td>
        <td></td>
        <td></td>
        <td>unieke taxa in de tentoonstellingsobjecten-lijst</td>
      </tr>
      <tr>
        <td>CRS</td>
        <td>{{ $tableData['crs']['numRows'] }} ({{ $tableData['crs']['numSpecimens'] }})</td>
        <td>{{ $tableData['crs']['harvestDate'] }}</td>
        <td>afbeeldingen uit het CRS</td>
      </tr>
      <tr>
        <td>Brahms</td>
        <td>{{ $tableData['brahms']['numRows'] }} ({{ $tableData['brahms']['numSpecimens'] }})</td>
        <td>{{ $tableData['brahms']['harvestDate'] }}</td>
        <td>afbeeldingen uit Brahms</td>
      </tr>
      <tr>
        <td>Iucn</td>
        <td>{{ $tableData['iucn']['numRows'] }}</td>
        <td>{{ $tableData['iucn']['harvestDate'] }}</td>
        <td>IUCN statussen (klein aantal soorten heeft meer dan één status)</td>
      </tr>
      <tr>
        <td>Topstukken</td>
        <td>{{ $tableData['topstukken']['numRows'] }}</td>
        <td>{{ $tableData['topstukken']['harvestDate'] }}</td>
        <td>topstuk-objecten</td>
      </tr>
      <tr>
        <td>Linnaeus (TTIK)</td>
        <td>{{ $tableData['ttik']['numRows'] }}</td>
        <td>{{ $tableData['ttik']['harvestDate'] }}</td>
        <td>ttik-records, één per (hoger) taxon</td>
      </tr>
      <tr>
        <td>Leenobjecten</td>
        <td>{{ $tableData['leenobjecten']['numRows'] }}</td>
        <td>{{ $tableData['leenobjecten']['harvestDate'] }}</td>
        <td>aantal leenobjecten (hopelijk met afbeeldingen)</td>
      </tr>
    </table>
  </div>

</x-app-layout>
